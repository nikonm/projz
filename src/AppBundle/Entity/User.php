<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 29.03.16
 * Time: 17:43
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"})}, indexes={@ORM\Index(name="I_idp_identifier_idx", columns={"idp_identifier"})})
 * @ORM\Entity(repositoryClass="AppBundle\EntityRepository\UserRepo")
 */
class User {

    /**
     * @Groups("default")
     * @Type("integer")
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Type("string")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idpIdentifier;

    /**
     * @Groups("default")
     * @Type("string")
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @Groups("default")
     * @Type("string")
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @Type("string")
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $password;

    /**
     * @Groups("default")
     * @Type("string")
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    private $phone;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdpIdentifier()
    {
        return $this->idpIdentifier;
    }

    /**
     * @param mixed $idpIdentifier
     * @return User
     */
    public function setIdpIdentifier($idpIdentifier)
    {
        $this->idpIdentifier = $idpIdentifier;

        return $this;
    }

}