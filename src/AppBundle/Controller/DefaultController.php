<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Helper\ResponseHelper;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class DefaultController extends Controller
{
    /**
     * Hello
     *
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /**
     * Возвращает текущего пользователя
     *
     * @ApiDoc(
     *     description="Возвращает текущего пользователя",
     *     responseMap={
     *         200 = "AppBundle\Entity\User"
     *     }
     *  )
     * @Get("/me", name="meAction")
     */
    public function meAction()
    {
        /** @var TokenStorage $securityStorage */
        $securityStorage = $this->container->get('security.token_storage');
        $user = $securityStorage->getToken()->getUser()->getUser();
        $user = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:User')
            ->find($user->getId());

        return new ResponseHelper($user);
    }

    /**
     * @ApiDoc(
     *     description="Retrieve list of users.",
     *     statusCodes={
     *         400 = "Validation failed."
     *     },
     *     responseMap={
     *         200 = "array<AppBundle\Entity\User>",
     *         400 = {
     *             "class"="CommonBundle\Model\ValidationErrors",
     *             "parsers"={"Nelmio\ApiDocBundle\Parser\JmsMetadataParser"}
     *         }
     *     }
     *  )
     * @Get("/users", name="homepage2")
     */
    public function allAction()
    {
        return new ResponseHelper((new User)->setEmail('fdsf')->setPhone('543534'));
    }
//
//    /**
//     * @Route("/users/{id}", name="homepage3")
//     */
//    public function getAction($id)
//    {
//
//        return array('user' => $id);
//    }
}
