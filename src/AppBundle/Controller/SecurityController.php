<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 28.03.16
 * Time: 11:46
 */

namespace AppBundle\Controller;


use AppBundle\DTO\Auth\AuthRequestDTO;
use AppBundle\DTO\Auth\AuthResponseDTO;
use AppBundle\DTO\Auth\IdpAuthRequestDTO;
use AppBundle\Entity\User;
use AppBundle\EntityRepository\UserRepo;
use AppBundle\Helper\ResponseHelper;
use AppBundle\Service\Idp\IdpService;
use AppBundle\Service\SecuredUser;
use AppBundle\Service\UserProvider;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\View;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator;

class SecurityController extends Controller {

    /**
     * Логин
     *
     * @ApiDoc(
     *     description="Логин",
     *     input="AppBundle\DTO\Auth\AuthRequestDTO",
     *     output="AppBundle\DTO\Auth\AuthResponseDTO",
     *     statusCodes={
     *         400 = "Validation failed.",
     *         404 = "Validation failed.",
     *     }
     *  )
     * @Post("/secured/login", name="user_login")
     * @ParamConverter("auth", converter="fos_rest.request_body")
     */
    public function loginAction(AuthRequestDTO $auth)
    {
        /** @var UserProvider $userProvider */
        $userProvider = $this->container->get('app.user_provider');
        $authResponse = $userProvider->authorize($auth);

        return new ResponseHelper($authResponse);
    }

    /**
     * Idp логин
     *
     * @ApiDoc(
     *     description="Логин через внешние сервисы",
     *     input="AppBundle\DTO\Auth\IdpAuthRequestDTO",
     *     output="AppBundle\DTO\Auth\AuthResponseDTO",
     *     statusCodes={
     *         400 = "Validation failed.",
     *         404 = "Validation failed.",
     *     }
     *  )
     * @Post("/secured/idp/login", name="user_idp_login")
     * @ParamConverter("auth", converter="fos_rest.request_body")
     */
    public function idpLoginAction(IdpAuthRequestDTO $auth)
    {
        /** @var UserProvider $userProvider */
        $userProvider = $this->container->get('app.user_provider');
        $authResponse = $userProvider->idpAuthorize($auth);
        return new ResponseHelper($authResponse);
    }
}