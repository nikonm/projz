<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 28.03.16
 * Time: 17:19
 */

namespace AppBundle\Controller;


use AppBundle\DTO\ErrorDTO;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\Translation\Translator;

class ExceptionController extends \FOS\RestBundle\Controller\ExceptionController {

    /**
     * @return ErrorDTO
     */
    public function showAction (Request $request, $exception, DebugLoggerInterface $logger = null) {

        $format = $this->getFormat($request, $request->getRequestFormat());
        if ($format != 'json') {
            return parent::showAction($request, $exception, $logger);
        }

        $error = new ErrorDTO();
        $error->setCode($exception->getStatusCode());
        $error->setMessage($exception->getMessage());

        if ($this->container->hasParameter('fos_rest.exception.codes')) {
            $codes = $this->container->getParameter('fos_rest.exception.codes');
            if (isset($codes[$exception->getClass()])) {
                $error->setCode($codes[$exception->getClass()]);
            }
        }
        if ($this->container->has('translator')) {
            /** @var Translator $translator */
            $translator = $this->container->get('translator');
            $error->setMessage($translator->trans($error->getMessage()));
        }


        /** @var Serializer $serializer */
        $serializer = $this->container->get('jms_serializer');

        $errorString = $serializer->serialize($error, $format);

        return new Response($errorString, $error->getCode());
    }
}