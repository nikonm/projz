<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 30.03.16
 * Time: 11:11
 */

namespace AppBundle\DTO\Auth;

use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;

class AuthResponseDTO {

    /**
     * @Groups("default")
     * @Type("AppBundle\Entity\User")
     */
    private $user;


    /**
     * @Groups("default")
     * @Type("string")
     */
    private $token;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return AuthResponseDTO
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return AuthResponseDTO
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}