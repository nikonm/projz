<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 30.03.16
 * Time: 11:18
 */

namespace AppBundle\DTO\Auth;


use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;

class AuthRequestDTO {

    /**
     * @Groups("default")
     * @Type("string")
     */
    private $login;


    /**
     * @Groups("default")
     * @Type("string")
     */
    private $password;

    /**
     * @Groups("default")
     * @Type("string")
     */
    private $platform;

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     * @return AuthRequestDTO
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return AuthRequestDTO
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     * @return AuthRequestDTO
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

}