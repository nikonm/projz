<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 31.03.16
 * Time: 18:18
 */

namespace AppBundle\DTO\Auth;


use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;

class IdpAuthRequestDTO {

    /**
     * @Groups("default")
     * @Type("string")
     * @var string
     */
    private $token;

    /**
     * @Groups("default")
     * @Type("string")
     * @var string
     */
    private $platform;

    /**
     * @Groups("default")
     * @Type("string")
     * @var string
     */
    private $idp;

    /**
     * @Groups("default")
     * @Type("integer")
     * @var int
     */
    private $userId;

    /**
     * @return string
     */
    public function getIdp()
    {
        return $this->idp;
    }

    /**
     * @param string $idp
     * @return IdpAuthRequestDTO
     */
    public function setIdp($idp)
    {
        $this->idp = $idp;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     * @return IdpAuthRequestDTO
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return IdpAuthRequestDTO
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return IdpAuthRequestDTO
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

}