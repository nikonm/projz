<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 29.03.16
 * Time: 14:11
 */

namespace AppBundle\DTO;


use JMS\Serializer\Annotation\Type;

class ErrorDTO {


    /**
     * @Type("integer")
     */
    private $code;


    /**
     * @Type("string")
     */
    private $message;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return ErrorDTO
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return ErrorDTO
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

}