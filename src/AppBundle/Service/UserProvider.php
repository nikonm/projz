<?php

namespace AppBundle\Service;

use AppBundle\DTO\Auth\AuthRequestDTO;
use AppBundle\DTO\Auth\AuthResponseDTO;
use AppBundle\DTO\Auth\IdpAuthRequestDTO;
use AppBundle\Entity\User;
use AppBundle\Service\Idp\IdpService;
use AppBundle\Service\Idp\UserData;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class UserProvider implements UserProviderInterface {

    /** @var EntityManager  */
    private $em;

    /** @var JWTManager */
    private $jwtManager;

    /** @var EncoderFactory  */
    private $encodersFactory;
    
    /** @var \Doctrine\ORM\EntityRepository  */
    private $userRepository;

    /** @var  IdpService */
    private $idpService;

    public function __construct(EntityManager $em, $jwtManager, EncoderFactory $encodersFactory, IdpService $idpService)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->encodersFactory = $encodersFactory;
        $this->userRepository = $this->em->getRepository('AppBundle:User');
        $this->idpService = $idpService;
    }

    public function loadUserByUsername($userId)
    {
        /** @var User $user */
        if(!($user = $this->userRepository->find($userId))) {
            throw new HttpException(401);
        }
        return new SecuredUser($user);
    }

    public function refreshUser(UserInterface $user)
    {
        return new SecuredUser($user->getUser());
    }

    public function supportsClass($class)
    {
        return $class === 'AppBundle\Service\SecuredUser';
    }

    /**
     * @param IdpAuthRequestDTO $auth
     * @return AuthResponseDTO
     */
    public function idpAuthorize(IdpAuthRequestDTO $auth)
    {
        $idpService = $this->idpService->create($auth->getIdp(), $auth->getUserId());

        if (!($userData = $idpService->getUserData($auth->getToken()))) {
            throw new ValidatorException('Invalid user data');
        }

        $userData->setId($auth->getIdp() . '&' . $userData->getId());
        $user = $this->findIdpUser($userData);
        $jwt = $this->jwtManager->create(new SecuredUser($user));

        return (new AuthResponseDTO)
            ->setToken($jwt)
            ->setUser($user);
    }

    /**
     * @param UserData $userData
     * @return User
     */
    private function findIdpUser(UserData $userData)
    {
        $user = $this->userRepository->findOneBy([
           'idpIdentifier' => $userData->getId()
        ]);
        if (!$user) {
            $user = (new User)
                ->setName($userData->getName())
                ->setPassword(uniqid())
                ->setEmail($userData->getEmail())
                ->setIdpIdentifier($userData->getId());
            $this->em->persist($user);
            $this->em->flush($user);
        }

        return $user;
    }

    /**
     * @param AuthRequestDTO $auth
     * @return AuthResponseDTO
     */
    public function authorize(AuthRequestDTO $auth)
    {
        /** @var User|null $user */
        $user = $this->userRepository->findOneBy([
            'email' => $auth->getLogin(),
            'password' => $this->hashPassword($auth->getPassword())
        ]);
        if (!$user) {
            throw new ValidatorException('Invalid');
        }

        $jwt = $this->jwtManager->create(new SecuredUser($user));

        return (new AuthResponseDTO)
            ->setToken($jwt)
            ->setUser($user);
    }

    private function hashPassword($password)
    {
        $encoder = $this->encodersFactory->getEncoder('sha512');
        return $encoder->encodePassword($password, '');
    }

}