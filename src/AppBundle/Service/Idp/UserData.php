<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ershov
 * Date: 15.08.2014
 * Time: 21:30
 */

namespace AppBundle\Service\Idp;

class UserData
{

    protected $id;

    protected $name;

    protected $email;

    public function __construct($id, $name, $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return UserData
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return UserData
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return UserData
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}