<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 31.03.16
 * Time: 18:53
 */

namespace AppBundle\Service\Idp;


use Symfony\Component\Validator\Exception\ValidatorException;

class IdpService {


    /** @var \GuzzleHttp\Client  */
    private $client;

    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
    }

    const IDP_FACEBOOK = 'facebook';
    const IDP_VK = 'vk';
    const IDP_GOOGLE = 'google';

    /**
     * @param $idpKey
     * @param null $userId
     * @return Driver\IdPBase
     */
    public function create($idpKey, $userId = null)
    {
        switch ($idpKey) {
            case self::IDP_FACEBOOK:
                return new Driver\FacebookIdP($this->client);

            case self::IDP_GOOGLE:
                return new Driver\GoogleIdP($this->client);

            case self::IDP_VK:
                return new Driver\VKIdP($this->client, $userId);

            default:
                throw new ValidatorException('Invalid idp provider');
        }
    }

}