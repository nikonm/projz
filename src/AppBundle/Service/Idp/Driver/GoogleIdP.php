<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ershov
 * Date: 15.08.2014
 * Time: 21:09
 */
namespace AppBundle\Service\Idp\Driver;

use AppBundle\Service\Idp\UserData;

class GoogleIdP extends IdPBase
{

    const EMAIL_FIELD = "email";
    const ID_FIELD = "id";
    const NAME_FIELD = "name";
    const GIVEN_NAME_FIELD = "given_name";
    const FAMILY_NAME_FIELD = "family_name";

    protected function getIdPUrl($token)
    {
        return "https://www.googleapis.com/oauth2/v2/userinfo?access_token=" . $token;
    }

    /**
     * @param array $data
     * @return UserData
     */
    protected function extractUserData($data)
    {

        $identifier = $this->getField($data, [
            self::ID_FIELD,
            self::EMAIL_FIELD
        ]);
        if ($identifier === null) {
            return null;
        }

        $name = $this->getField($data, [
            self::NAME_FIELD,
            self::GIVEN_NAME_FIELD,
            self::FAMILY_NAME_FIELD
        ], $identifier);

        $email = $this->getField($data, [self::EMAIL_FIELD]);

        $userData = new UserData($identifier, $name, $email);

        return $userData;
    }
}