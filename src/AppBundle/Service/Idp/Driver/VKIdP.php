<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ershov
 * Date: 15.08.2014
 * Time: 21:08
 */

namespace AppBundle\Service\Idp\Driver;

use AppBundle\Service\Idp\UserData;

class VKIdP extends IdPBase
{

    const EMAIL_FIELD = "email";
    const UID_FIELD = "uid";
    const NICKNAME_FIELD = "nickname";
    const FIRST_NAME_FIELD = "first_name";
    const LAST_NAME_FIELD = "last_name";

    protected function getIdPUrl($token)
    {
        return "https://api.vk.com/method/users.get?uids={$this->userId}&fields=uid,first_name,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo&access_token=$token";
    }

    /**
     * @param array $data
     * @return UserData
     */
    protected function extractUserData($data)
    {
        $data = $data["response"][0];

        $identifier = $this->getField($data, [
            self::EMAIL_FIELD,
            self::UID_FIELD
        ]);
        if ($identifier === null) {
            return null;
        }

        $name = $this->getField($data, [
            self::NICKNAME_FIELD,
            self::FIRST_NAME_FIELD,
            self::LAST_NAME_FIELD
        ], $identifier);

        $email = $this->getField($data, [self::EMAIL_FIELD]);

        $userData = new UserData($identifier, $name, $email);

        return $userData;
    }

    protected function validateData($data)
    {
        if (!parent::validateData($data)) {
            return false;
        }

        if (array_key_exists('error', $data)) {
            if (array_key_exists('error_msg', $data['error'])) {
                $this->errorMsg = $data['error']['error_msg'];
            }
            $this->errorMsg = $data['error']['error_msg'];

            return false;
        }

        if (!array_key_exists('response', $data) || !is_array($data['response']) || count($data['response']) < 1) {
            return false;
        }

        return true;
    }
}