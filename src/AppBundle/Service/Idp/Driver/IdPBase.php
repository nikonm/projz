<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ershov
 * Date: 15.08.2014
 * Time: 21:00
 */

namespace AppBundle\Service\Idp\Driver;

use AppBundle\Service\Idp\UserData;
use GuzzleHttp\ClientInterface;

abstract class IdPBase
{
    /** @var ClientInterface  */
    protected $client;

    protected $userId;

    protected $errorMsg;

    public function __construct(ClientInterface $client, $userId = null)
    {
        $this->client = $client;
        $this->userId = $userId;
    }

    protected abstract function getIdPUrl($token);

    /**
     * @param array $data
     * @return UserData
     */
    protected abstract function extractUserData($data);

    /**
     * @param string $token
     * @return null|UserData
     */
    public function getUserData($token)
    {
        $idpUrl = $this->getIdPUrl($token);
        $response = $this->client->get($idpUrl);
        $rawData = $response->getBody()->getContents();

        if ($rawData === false) {
            return null;
        }

        $data = json_decode($rawData, true);
        if (!$this->validateData($data)) {
            return null;
        }

        return $this->extractUserData($data);
    }

    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    protected function validateData($data)
    {
        return $data !== false && is_array($data);
    }

    protected function getField($data, $candidates, $defaultValue = null)
    {
        foreach ($candidates as $candidate) {
            if (array_key_exists($candidate, $data) && !empty($data[$candidate])) {
                return $data[$candidate];
            }
        }

        return $defaultValue;
    }
} 