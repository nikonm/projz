<?php
/**
 * Created by IntelliJ IDEA.
 * User: Ershov
 * Date: 15.08.2014
 * Time: 21:04
 */

namespace AppBundle\Service\Idp\Driver;

use AppBundle\Service\Idp\UserData;

class FacebookIdP extends IdPBase
{

    const EMAIL_FIELD = "email";
    const ID_FIELD = "id";
    const NAME_FIELD = "name";
    const FIRST_NAME_FIELD = "first_name";
    const LAST_NAME_FIELD = "last_name";

    protected function getIdPUrl($token)
    {
        return "https://graph.facebook.com/me?access_token=" . $token;
    }

    /**
     * @param array $data
     * @return UserData
     */
    protected function extractUserData($data)
    {

        $identifier = $this->getField($data, [
            self::EMAIL_FIELD,
            self::ID_FIELD
        ]);
        if ($identifier === null) {
            return null;
        }

        $name = $this->getField($data, [
            self::NAME_FIELD,
            self::FIRST_NAME_FIELD,
            self::LAST_NAME_FIELD
        ], $identifier);
        $email = $this->getField($data, [
            self::EMAIL_FIELD
        ]);

        $userData = new UserData($identifier, $name, $email);

        return $userData;
    }
}