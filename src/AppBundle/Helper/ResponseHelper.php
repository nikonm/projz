<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 29.03.16
 * Time: 16:14
 */

namespace AppBundle\Helper;


use Symfony\Component\HttpFoundation\ParameterBag;

class ResponseHelper {

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var []
     */
    private $headers;

    /**
     * @var string
     */
    private $context;

    public function __construct($data = null, $context = null, $statusCode = 200, $headers = [])
    {
        $this->data = $data;
        $this->context = $context;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return ResponseHelper
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param [] $headers
     * @return ResponseHelper
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return ResponseHelper
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param string $context
     * @return ResponseHelper
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }
}