<?php
/**
 * Created by IntelliJ IDEA.
 * User: nikon
 * Date: 29.03.16
 * Time: 16:07
 */

namespace AppBundle\Handler;


use AppBundle\Helper\ResponseHelper;
use JMS\Serializer\Exception\ValidationFailedException;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;


class EventSubscriber implements EventSubscriberInterface {

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct($serializer)
    {
        $this->serializer = $serializer;
    }

    public function onRequest (GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }
        $request = $event->getRequest();
        $request->setLocale($request->getPreferredLanguage());
    }

    public function onView(GetResponseForControllerResultEvent $event)
    {
        $controllerResult = $event->getControllerResult();
        if ($controllerResult instanceof ResponseHelper) {
            $context = SerializationContext::create();
            $context->setGroups('default');
            if ($contextController = $controllerResult->getContext()) {

                $context->setGroups($contextController);
            }
            $data = $this->serializer->serialize($controllerResult->getData(), 'json', $context ?: null);
            $event->setResponse(new Response($data, $controllerResult->getStatusCode(), $controllerResult->getHeaders()));
            $event->stopPropagation();
            return;
        }
        return;
    }

    public function onControllerValidate (FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $errors = $request->attributes->get('validationErrors');
        if (count($errors) > 0) {
            throw new ValidationFailedException($errors);
        }

    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'onRequest',
            KernelEvents::VIEW => 'onView',
            KernelEvents::CONTROLLER => [
                ['onControllerValidate', -1]
            ]
        ];
    }


}